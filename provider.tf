# указываем terraform, какой облачный провайдер будем использовать
provider "google" {
  credentials = file("admin-key.json") # путь до файла с кредами для взаимодействия с API GCP
  project     = "braided-shift-330610" # ID проекта, который создали в GCP
  region      = "europe-central2"      # регион, где хотим создать тачки
  zone        = "europe-central2-a"    # зона, где хотим создать тачки
}