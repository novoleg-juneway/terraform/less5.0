resource "google_compute_instance" "vm1" {
  count        = 3
  name         = "node-${count.index+1}"
  machine_type = "e2-small"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size  = 20
    }
  }

  metadata = {
    ssh-keys = "novoleg2697:${file("~/.ssh/id_rsa.pub")}"
  }


  network_interface {
    # A default network is created for all GCP projects
    network = "default"
    access_config {
    }
  }
}